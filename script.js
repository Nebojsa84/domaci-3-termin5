$(document).ready(function(){

    var apiKey = '3726fe9cef4ab9d506ee77d8d8f7f3d1';
    var rest_url = 'https://api.openweathermap.org/data/2.5/forecast?';
    var units = 'metric';
  
    $("#btn").click(function(){
        $("#tbl").empty();
        var name=$('#inpt').val();
        var request_url= rest_url + 'q=' + name + '&units=' + units + '&appid=' + apiKey; 

        $.get(request_url,showData)
        .fail(function() {
            alert( "" );
          })
    });


function showData(data){
       
    var city_name = ("<h3>City name: "+data.city.name+"</h3>")
    var city_id = ("<h3>City ID: "+data.city.id+"</h3>")
    var country = ("<h3>Country code: "+data.city.country+"</h3>")
    var population= ("<h3>Population: "+data.city.population+"</h3>")
    $("#city").append(city_name,city_id,country,population)
      
    var item =[]
    for(i=0; i<data.list.length; i++){
        
        if(item.length<1){
            item.push(data.list[i])
            var date= data.list[i].dt_txt.split(" ")[0];
            var time=data.list[i].dt_txt.split(" ")[1];

            var table = $('<table></table>').addClass('table table-bordered ');   

            table.append($('<tr>')
                 .append($('<th colspan="5" >').append("Date: "+date )))
                 .append($('<tr>') 
                 .append($('<th>').append("Time"))
                 .append($('<th>').append("Temperature "))  
                 .append($('<th>').append("Description "))
                 .append($('<th>').append("Humidity"))  
                 .append($('<th>').append("Pressure")))
                 .append($('<tr>') 
                 .append($('<td>').append(time))
                 .append($('<td>').append(data.list[i].main.temp))  
                 .append($('<td>').append(data.list[i].weather[0].description))
                 .append($('<td>').append(data.list[i].main.humidity))  
                 .append($('<td>').append(data.list[i].main.pressure))                
                ); 
      
        }
         else{
            var date= data.list[i].dt_txt.split(" ")[0];
            var time=data.list[i].dt_txt.split(" ")[1];
            date2=item[0].dt_txt.split(" ")[0]

            if(date===date2){
                item.push(data.list[i])
                table.append($('<tr>') 
                .append($('<td>').append(time))
                .append($('<td>').append(data.list[i].main.temp))  
                .append($('<td>').append(data.list[i].weather[0].description))
                .append($('<td>').append(data.list[i].main.humidity))  
                .append($('<td>').append(data.list[i].main.pressure))                
                ); 

            }else{
                table.append($('<tr>') 
                .append($('<td>').append(time))
                .append($('<td>').append(data.list[i].main.temp))  
                .append($('<td>').append(data.list[i].weather[0].description))
                .append($('<td>').append(data.list[i].main.humidity))  
                .append($('<td>').append(data.list[i].main.pressure))                
                ); 
                item=[]               
            }

            $("#tbl").append(table)
         }          
        
    }              
   
    };

});